package com.tryit.tryit;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.tryit.tryit.fragments.ContentFragment;
import com.tryit.tryit.fragments.EstatisticsFragment;
import com.tryit.tryit.fragments.ExerciseFragment;
import com.tryit.tryit.fragments.WelcomeFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private WelcomeFragment welcomeFragment;
    private ContentFragment contentFragment;
    private ExerciseFragment exerciseFragment;
    private EstatisticsFragment estatisticsFragment;

    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferences = getSharedPreferences("user", Context.MODE_PRIVATE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        String name = preferences.getString("name", null);
        String email = preferences.getString("email", null);

        /*if(name != null) {
            TextView nameTextView = navigationView.findViewById(R.id.name);
            nameTextView.setText(name);
        }

        if(email != null) {
            TextView emailTextView = navigationView.findViewById(R.id.email);
            emailTextView.setText(email);
        }*/



        if(savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, new WelcomeFragment()).commit();
        }

    }

    public void openFragment(Fragment fragment) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        // Add the fragment to the 'fragment_container' FrameLayout
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);

        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public WelcomeFragment getWelcomeFragment() {
        if(welcomeFragment == null) {
            welcomeFragment = new WelcomeFragment();
            String name = preferences.getString("name", "Nome não informado");
            String email = preferences.getString("email", "Email não informado");
            Bundle args = new Bundle();
            args.putString("name", name);
            args.putString("email", email);
            welcomeFragment.setArguments(args);
        }
        return welcomeFragment;
    }

    public EstatisticsFragment getEstatisticsFragment() {
        if(estatisticsFragment == null) {
            estatisticsFragment = new EstatisticsFragment();
        }
        return estatisticsFragment;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if(id == R.id.nav_welcome) {
            openFragment(getWelcomeFragment());
        } else if (id == R.id.nav_content) {
            if(contentFragment == null) {
                contentFragment = new ContentFragment();
            }
            openFragment(contentFragment);
        } else if (id == R.id.nav_exercises) {
            if(exerciseFragment == null) {
                exerciseFragment = new ExerciseFragment();
            }
            openFragment(exerciseFragment);
        } else if (id == R.id.nav_estatistics) {
            openFragment(getEstatisticsFragment());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
