package com.tryit.tryit.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tryit.tryit.R;

public class EstatisticsFragment extends Fragment {


    private TextView progress;
    private TextView totalAnswers;
    private TextView rightAnswers;
    private TextView rightAnswersRatio;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_estatistics, container, false);

        this.progress = view.findViewById(R.id.progress);
        this.totalAnswers = view.findViewById(R.id.total_answers);
        this.rightAnswers = view.findViewById(R.id.right_answers);
        this.rightAnswersRatio= view.findViewById(R.id.right_answers_ratio);

        this.progress.setText("100%");
        this.totalAnswers.setText("4");
        this.rightAnswers.setText("3");
        this.rightAnswersRatio.setText("75%");

        return view;
    }
}