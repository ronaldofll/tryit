package com.tryit.tryit.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tryit.tryit.R;

public class WelcomeFragment extends Fragment {

    private EditText nameEditText;
    private EditText emailEditText;

    private TextView nameTextVIew;

    private LinearLayout welcome;
    private LinearLayout introduction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_welcome, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.nameEditText = view.findViewById(R.id.name_edit_text);
        this.emailEditText = view.findViewById(R.id.email_edit_text);
        this.nameTextVIew = view.findViewById(R.id.welcome_message_text);

        this.welcome = view.findViewById(R.id.welcome_message);
        this.introduction = view.findViewById(R.id.introduction_message);

        final SharedPreferences sharedPreferences = getActivity().getSharedPreferences("user", 0);

        String name = sharedPreferences.getString("name", null);
        if(name != null) {
            TextView textView = view.findViewById(R.id.welcome_message_text);
            textView.setText(getString(R.string.welcome_message_name, name));

            welcome.setVisibility(View.VISIBLE);
            introduction.setVisibility(View.GONE);
        } else {
            view.findViewById(R.id.login_layout).setVisibility(View.VISIBLE);
            view.findViewById(R.id.save_login_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("name", nameEditText.getText().toString());
                    editor.putString("email", emailEditText.getText().toString());
                    editor.apply();

                    nameTextVIew.setText(getString(R.string.welcome_message_name, nameEditText.getText().toString()));

                    welcome.setVisibility(View.VISIBLE);
                    introduction.setVisibility(View.GONE);
                }
            });
        }

    }
}
