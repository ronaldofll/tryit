package com.tryit.tryit.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.tryit.tryit.MainActivity;
import com.tryit.tryit.R;
import com.tryit.tryit.com.tryit.tryit.components.ContentItem;
import com.tryit.tryit.com.tryit.tryit.components.ExerciseItem;

import java.util.ArrayList;
import java.util.List;

public class ExerciseFragment extends Fragment {

    MainActivity mainActivity;

    LinearLayout contentList;
    LinearLayout exerciseContent;
    LinearLayout exerciseMenu;

    List<ExerciseItem> itens;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mainActivity = (MainActivity) context;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_exercise, container, false);
        this.contentList = view.findViewById(R.id.exercise_list);
        this.itens = new ArrayList<>();

        exerciseContent = view.findViewById(R.id.exercise_content);
        exerciseMenu = view.findViewById(R.id.exercise_menu);

        Button submit = view.findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int totalCont = itens.size();
                int rightAnswers = 0;

                for(ExerciseItem exerciseItem : itens) {
                    if(exerciseItem.validate()) {
                        rightAnswers++;
                    }
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage("Você acertou " + rightAnswers + " de " + totalCont + "("+ ((int) (100 * (((double) rightAnswers)/((double) totalCont)))) +"%)");
                builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mainActivity.openFragment(mainActivity.getEstatisticsFragment());
                    }
                });
                builder.show();
            }
        });

        populateMenu();

        return view;
    }

    private void populateMenu() {
        for(int i = 0; i < 10; i++) {
            Button button = new Button(getContext());
            button.setText("Exercícios " + (i+1));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 200);
            params.setMargins(50, 10, 50, 0);
            button.setLayoutParams(params);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    exerciseMenu.setVisibility(View.GONE);
                    exerciseContent.setVisibility(View.VISIBLE);
                }
            });
            exerciseMenu.addView(button);

        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ExerciseItem e1 = new ExerciseItem(this, "Exercício 1");
        e1.setDescription("Qual dos items abaixo NÃO faz parte de uma operação lógica");
        e1.addWrongOption("<");
        e1.addWrongOption("==");
        e1.addWrongOption("!=");
        e1.addRightOption("=");
        itens.add(e1);

        ExerciseItem e2 = new ExerciseItem(this, "Exercício 2");
        e2.setDescription("Escolha a melhor opção de loop para repetir 10x uma ação");
        e2.addWrongOption("Enquanto (while)");
        e2.addWrongOption("Repita (do)");
        e2.addRightOption("Para (for)");
        e2.addWrongOption("Se (if)");
        itens.add(e2);

        ExerciseItem e3 = new ExerciseItem(this, "Exercício 3");
        e3.setDescription("Assinale a alternativa correta. Com o uso de algoritmos, é possível realizar tarefas como: ");
        e3.addWrongOption("Avaliar expressões algébricas, relacionais e lógicas");
        e3.addWrongOption("Tomar decisões com base nos resultados das expressões avaliadas");
        e3.addWrongOption("Repetir um conjunto de ações de acordo com uma condição");
        e3.addRightOption("Todas as alternativas");
        itens.add(e3);

        ExerciseItem e4 = new ExerciseItem(this, "Exercício 4");
        e4.setDescription("Seguindo a lógica de algoritmos, qual seria a sequência correta para realizar a troca de um pneu de carro?");
        e4.addWrongOption("Desenroscar os 4 parafusos do pneu furado > Pegar as ferramentas (chave e macaco) > Pegar o estepe > Suspender o carro com o macaco > Colocar o estepe > Enroscar os 4 parafusos > Baixar o carro com o macaco > Guardar as ferramentas > Desligar o carro");
        e4.addWrongOption("Pegar o estepe > Colocar o estepe > Desligar o carro > Baixar o carro com o macaco > Pegar as ferramentas (chave e macaco) > Suspender o carro com o macaco > Desenroscar os 4 parafusos do pneu furado > Enroscar os 4 parafusos > Guardar as ferramentas");
        e4.addWrongOption("Desligar o carro > Pegar as ferramentas (chave e macaco) > Pegar o estepe > Suspender o carro com o macaco > Desenroscar os 4 parafusos do pneu furado > Colocar o estepe > Enroscar os 4 parafusos > Baixar o carro com o macaco > Guardar as ferramentas");
        e4.addWrongOption("Pegar as ferramentas (chave e macaco) > Suspender o carro com o macaco > Desligar o carro > Desenroscar os 4 parafusos do pneu furado > Colocar o estepe > Enroscar os 4 parafusos > Baixar o carro com o macaco > Guardar as ferramentas > Pegar o estepe");
        e4.addWrongOption("Desligar o carro > Desenroscar os 4 parafusos do pneu furado > Guardar as ferramentas > Pegar o estepe > Suspender o carro com o macaco > Pegar as ferramentas (chave e macaco) > Colocar o estepe > Enroscar os 4 parafusos > Baixar o carro com o macaco");
        itens.add(e4);

        updateList();
    }

    public void updateList() {
        contentList.removeAllViews();
        for(ExerciseItem exerciseItem : itens) {
            this.contentList.addView(exerciseItem.getView());
        }
    }
}
