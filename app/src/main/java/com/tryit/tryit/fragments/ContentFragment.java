package com.tryit.tryit.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.tryit.tryit.R;
import com.tryit.tryit.com.tryit.tryit.components.ContentItem;

public class ContentFragment extends Fragment {

    LinearLayout contentList;
    LinearLayout contentMenu;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_content, container, false);
        this.contentList = view.findViewById(R.id.content_list);
        this.contentMenu = view.findViewById(R.id.content_menu);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ContentItem c1 = new ContentItem(this, "Introdução aos algoritmos");

        c1.addTextContent("O que são algoritmos e por que você deveria se importar com eles? " +
                "Vamos começar com uma visão geral de algoritmos, e então discutir sobre dois " +
                "jogos nos quais você poderia usar um algoritmo para resolvê-los de forma mais " +
                "eficiente - o jogo da adivinhação do número, e o jogo de encontrar uma rota.");

        c1.addImageContent(R.drawable.algoritmo);

        ContentItem c2 = new ContentItem(this, "Exemplo de algorítimo");
        c2.addTextContent("Imagine o trabalho de um recepcionista de cinema, ele deve conferir " +
                "os bilhetes e direcionar o cliente para a sala correta. Além disso, se o " +
                "cliente estiver 30 minutos adiantado o recepcionista deve informar que a sala " +
                "do filme ainda não está aberta. E quando o cliente estiver 30 minutos atrasado " +
                "o recepcionista deve informar que a entrada não é mais permitida");

        c1.updateView();
        c2.updateView();

        this.contentList.addView(c1.getView());
        this.contentList.addView(c2.getView());

        populateMenu();

    }

    private void populateMenu() {
        for(int i = 0; i < 10; i++) {
            Button button = new Button(getContext());
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 200);
            params.setMargins(50, 10, 50, 0);
            button.setLayoutParams(params);
            button.setText("Conteúdo " + (i+1));
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    contentMenu.setVisibility(View.GONE);
                    contentList.setVisibility(View.VISIBLE);
                }
            });
            contentMenu.addView(button);

        }
    }
}
