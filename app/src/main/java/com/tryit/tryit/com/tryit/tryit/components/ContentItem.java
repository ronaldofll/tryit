package com.tryit.tryit.com.tryit.tryit.components;

import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tryit.tryit.R;

import java.util.ArrayList;
import java.util.List;

public class ContentItem {

    private View view;

    //private String subject;

    private List<View> contentViewList;

    private TextView subjectTextView;
    private LinearLayout textsLinearLayout;

    public ContentItem(Fragment fragment, String subject) {
        init(fragment, subject);
    }

    private void init(Fragment fragment, String subject) {
        //this.subject = subject;
        this.contentViewList = new ArrayList<>();

        this.view = fragment.getLayoutInflater().inflate(R.layout.content_item, null, false);
        this.subjectTextView = view.findViewById(R.id.subject);
        this.textsLinearLayout = view.findViewById(R.id.contentList);

        subjectTextView.setText(subject);

    }
    public void updateView() {
        textsLinearLayout.removeAllViews();
        for(View view : contentViewList) {
            textsLinearLayout.addView(view);
        }
    }

    public void addTextContent(String text) {
        text = "\t" + text;
        TextView textView = new TextView(view.getContext());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(50, 0, 10, 20);
        textView.setLayoutParams(params);
        textView.setText(text);
        contentViewList.add(textView);
    }

    public void addImageContent(int imageResourceId) {
        ImageView imageView = new ImageView(view.getContext());
        imageView.setImageResource(imageResourceId);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(50, 10, 10, 40);
        params.gravity = Gravity.LEFT;
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setAdjustViewBounds(true);
        imageView.setLayoutParams(params);
        contentViewList.add(imageView);
    }

    public void addSubItemContent(ContentItem contentItem) {
        contentItem.updateView();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.setMargins(50, 0, 10, 0);
        contentItem.getView().setLayoutParams(params);
        contentViewList.add(contentItem.getView());
    }

    public View getView() {
        return this.view;
    }

}
