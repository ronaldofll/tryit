package com.tryit.tryit.com.tryit.tryit.components;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.tryit.tryit.R;

import java.util.ArrayList;
import java.util.List;

public class ExerciseItem {

    private View view;

    private TextView descriptionTextView;
    private RadioGroup optionsRadioGroup;

    private boolean validate = false;

    public ExerciseItem(Fragment fragment, String title) {
        init(fragment, title, null);
    }

    public void setDescription(String description) {
        descriptionTextView.setText(description);
    }

    private RadioButton createOption(String text) {
        RadioButton radioButton = new RadioButton(view.getContext());
        radioButton.setText(text);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 0, 0, 0);
        radioButton.setLayoutParams(params);
        return radioButton;
    }

    public void addWrongOption(String text) {
        RadioButton radioButton = createOption(text);
        this.optionsRadioGroup.addView(radioButton);
    }

    public void addRightOption(String text) {
        RadioButton radioButton = createOption(text);
        radioButton.isRightAnswer = true;

        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton rButton, boolean b) {
                if(rButton.isChecked()) {
                    validate = true;
                }
            }
        });

        this.optionsRadioGroup.addView(radioButton);
    }

    public boolean validate() {
        return validate;
    }

    private void init(Fragment fragment, String title, Integer imgResId) {
        //this.subject = subject;

        this.view = fragment.getLayoutInflater().inflate(R.layout.exercise_item, null, false);


        TextView titleTextView = view.findViewById(R.id.title);
        this.descriptionTextView = view.findViewById(R.id.description);
        ImageView imageView = view.findViewById(R.id.image);
        this.optionsRadioGroup = view.findViewById(R.id.options);

        if(imgResId != null) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageResource(imgResId);
        }

        titleTextView.setText(title);
    }

    public View getView() {
        return view;
    }

    private class RadioButton extends AppCompatRadioButton {
        private boolean isRightAnswer = false;
        public RadioButton(Context context) {
            super(context);
        }
    }
}
